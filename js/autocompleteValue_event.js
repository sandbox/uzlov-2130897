(function($) {

  Drupal.behaviors.autocompleteValue_event = {
    attach: function(context, settings) {
      if (Drupal.jsAC) {
        Drupal.jsAC.prototype.hidePopup = function(keycode) {
          // Select item if the right key or mousebutton was pressed.
          if (this.selected && ((keycode && keycode != 46 && keycode != 8 && keycode != 27) || !keycode)) {
            this.input.value = $(this.selected).data('autocompleteValue');
            // Custom: add an event trigger
            $(this.input).trigger('autocompleteSelect');
          }
          // Hide popup.
          var popup = this.popup;
          if (popup) {
            this.popup = null;
            $(popup).fadeOut('fast', function() {
              $(popup).remove();
            });
          }
          this.selected = false;
          $(this.ariaLive).empty();
        };
      }
    }
  };

})(jQuery);
