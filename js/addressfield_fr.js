(function($) {

  Drupal.behaviors.addressfield_fr = {
    attach: function(context, settings) {
      var autocomplete_city = Drupal.settings.AddressFieldFR.AutocompleteCity;
      var autocomplete_postal_code = Drupal.settings.AddressFieldFR.AutocompletePostalCode;
      var city_query = Drupal.settings.AddressFieldFR.CityQuery;
      var postal_code_query = Drupal.settings.AddressFieldFR.PostalCodeQuery;

      if (city_query) {
        $('.field-type-addressfield .addressfield-fr input.postal-code', context).each(function() {
          $(this).bind('autocompleteSelect', function() {
            var PostalCodeAndСity = $('#autocomplete li.selected').text();
            var City = PostalCodeAndСity.substr(7);
            var PostalCode = PostalCodeAndСity.substr(0, 5);
            $(this).val(PostalCode).parent().parent().find('.locality').val(City);
          })
        })
      }

      if (postal_code_query) {
        $('.field-type-addressfield .addressfield-fr input.locality', context).each(function() {
          $(this).bind('autocompleteSelect', function() {
            var СityAndPostalCode = $('#autocomplete li.selected').text();
            var City = СityAndPostalCode.substr(0, СityAndPostalCode.length - 7);
            var PostalCode = СityAndPostalCode.substr(СityAndPostalCode.length - 5);
            $(this).val(City).parent().parent().find('.postal-code').val(PostalCode);
          })
        })
      }

    }
  };

})(jQuery);
