<?php

/**
 * Form callback showing a form of configuration of Addressfield France plugin.
 * Form for upload a CSV file with french cities and postal codes.
 */
function addressfield_fr_admin_settings_form($form, &$form_state) {
  $form['browser'] = array(
    '#type' => 'fieldset',
    '#title' => t('Upload a CSV file'),
    '#description' => t('Upload a CSV file with french cities and postal codes.'),
  );

  $file_size = t('Maximum file size: !size MB.', array('!size' => file_upload_max_size()));

  $form['browser']['file_upload'] = array(
    '#type' => 'file',
    '#title' => t('CSV File'),
    '#size' => 40,
    '#description' => t('Select the CSV file to be imported. ') . $file_size,
  );

  $form['browser']['first_line'] = array(
    '#type' => 'checkbox',
    '#title' => t('Not import first line'),
    '#description' => t('Not import first line of CSV file.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  // Set the form encoding type.
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  return $form;
}

/**
 * Form validation callback of configuration of Addressfield France plugin.
 * Make sure that the file imported is of CSV type (check the mime type of the file uploaded).
 */
function addressfield_fr_admin_settings_form_validate($form, &$form_state) {
  // Attempt to save the uploaded file.
  $file = file_save_upload('file_upload', array(
    'file_validate_extensions' => array('txt csv'),
  ));

  // Check file uploaded OK.
  if (!$file) {
    form_set_error('file_upload', t('A file must be uploaded or selected from FTP updates.'));
  }
  else if ($file->filemime != 'text/csv') {
    form_set_error('file_upload', t('The file must be of CSV type only.'));
  }
  else {
    // Set files to form_state, to process when form is submitted.
    $form_state['values']['file_upload'] = $file;
  }
}

/**
 * Form submit callback of configuration of Addressfield France plugin.
 * Finally empty exist table with cities and get data from the csv.
 */
function addressfield_fr_admin_settings_form_submit($form, &$form_state) {
  $file_path = $form_state['values']['file_upload']->uri;
  $firstline = $form_state['values']['first_line'];

  // Clear exist content.
  $result = db_truncate('fr_cities')->execute();
  // Import new content.
  _create_batch_operations_for_import_csv($file_path, 'admin/config/content/addressfield_fr', $firstline);
}
