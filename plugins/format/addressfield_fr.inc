<?php

/**
 * @file
 * The default format for France adresses.
 */
$plugin = array(
  'title' => t('Address form (specific for France)'),
  'format callback' => 'addressfield_fr_format_address_generate',
  'type' => 'address',
  'weight' => -100,
);

/**
 * Format callback for France address.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_fr_format_address_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'FR') {



    $format['locality_block']['#attributes'] = array(
      'class' => array(
        'addressfield-container-inline',
        'locality-block',
        'addressfield-fr',
      ),
    );

    drupal_add_js(array('AddressFieldFR' => array(
        'AutocompleteCity' => $context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_city'],
        'AutocompletePostalCode' => $context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_postal_code'],
        'CityQuery' => $context['instance']['widget']['settings']['addressfield_fr_options']['city_query'],
        'PostalCodeQuery' => $context['instance']['widget']['settings']['addressfield_fr_options']['postal_code_query'],
      )), 'setting');

    // Enable the autocomplete for cities names.
    if ($context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_city']) {
      $format['locality_block']['locality']['#autocomplete_path'] = 'addressfield_fr/city_name/autocomplete';
    }
    // Enable the autocomplete for postal codes.
    if ($context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_postal_code']) {
      $format['locality_block']['postal_code']['#autocomplete_path'] = 'addressfield_fr/postal_code/autocomplete';
    }
    // Add js for the autocomplete.
    if ($context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_city'] ||
        $context['instance']['widget']['settings']['addressfield_fr_options']['autocomplete_postal_code']) {
      drupal_add_js(drupal_get_path('module', 'addressfield_fr') . '/js/autocompleteValue_event.js');
      drupal_add_js(drupal_get_path('module', 'addressfield_fr') . '/js/addressfield_fr.js');
    }

    if ($context['instance']['widget']['settings']['addressfield_fr_options']['po_box']) {

      if ($context['mode'] == 'form') {
        $format['street_block']['po_box'] = array(
          '#title' => t('P.O. box'),
          '#type' => 'textfield',
          '#size' => 20,
          '#attributes' => array('class' => array('po-box-field')),
          '#default_value' => isset($address['po_box']) ? $address['po_box'] : '',
          '#weight' => 1,
        );
      }
      else {
        // Add our own render callback for the format view.
        $format['#pre_render'][] = '_addressfield_fr_pobox_render_address';
      }
    }

    // Refresh form when user select France in country list.
    if ($context['mode'] == 'form' && $address['country'] == 'FR') {
      $format['locality_block']['locality']['#ajax'] = array(
        'callback' => 'addressfield_standard_widget_refresh',
        'wrapper' => $format['#wrapper_id'],
        'method' => 'replace',
      );
    }
    else {
      if (isset($format['locality_block']['locality'])) {
        unset($format['locality_block']['locality']['#ajax']);
      }
    }
  }
}
